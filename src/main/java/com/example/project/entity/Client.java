package com.example.project.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


import lombok.Data;

@Entity
@Data
public class Client {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idClient;
	
	@Column
	private String nomClient ;
	
	@Column
	private String prenomClient;
	
	@Column
	private LocalDate dateDeNaissanceClient;
	
	@Column
	private String courrielClient ;
	
	@Column
	private String telClient;
	
	@OneToMany(mappedBy = "client" ,fetch = FetchType.LAZY)
	private List<Ticket> tickets ;
}
