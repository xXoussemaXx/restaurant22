package com.example.project.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Entity
@Data
public class Tablee {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long numTable;
	
	@Column
	private int nbCouvert;
	
	@Column
	private String typeTable;
	
	@Column
	private float supplement ;
	
	@OneToMany(mappedBy = "tablee",fetch = FetchType.LAZY)
	private List<Ticket> tickets;
}
