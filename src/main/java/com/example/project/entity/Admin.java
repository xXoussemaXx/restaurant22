package com.example.project.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Admin {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	private Long id ;
	
	@Column
	private String nomAdmin;
	
	@Column
	private String prenomAdmin;
	
	@Column
	private String mdpAdmin;
}
