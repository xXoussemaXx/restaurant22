package com.example.project.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
public class Ticket {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long numTicket;
	
	@Column
	private LocalDate dateTicket;
	
	@Column
	private int nbCouvert;
	
	@Column
	private float addition;
	
	@ManyToOne
	@JoinColumn(name = "id_client", referencedColumnName = "idClient")
	@JsonIgnore
	private Client client;
	
	@ManyToOne
	@JoinColumn(name = "id_tablee", referencedColumnName = "numTable")
	@JsonIgnore
	private Tablee tablee;


	

}
