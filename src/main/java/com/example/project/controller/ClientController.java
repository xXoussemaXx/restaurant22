package com.example.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.example.project.entity.Client;
import com.example.project.service.ClientService;





@Controller
@RequestMapping
public class ClientController {

	@Autowired
	private ClientService clientService;
	
	
	@GetMapping("/client")
	
	public String viewHomePage(Model model) {
		List<Client> listClients = clientService.findAllClient();
		model.addAttribute("listClients", listClients);
		
		return "gestionclient";
	}
	

	@GetMapping("/addclient")
	public String showClientForm(Model model) {
		Client client = new Client();
		model.addAttribute("client", client);
		return "clientform";
	}
	
	
	@PostMapping("/saveClient")
	public String saveClient( @ModelAttribute("client") Client client) {
		 clientService.saveClient(client);
		 return "redirect:/client";
	}
	
	@GetMapping("/updateclient/{idClient}")
	public String showClientFormForUpdate(@PathVariable(value = "idClient") Long idClient ,Model model) {
		Client client = clientService.findClientById(idClient);
		model.addAttribute("client", client);
		return "clientupdate";
	}
	
	@GetMapping("/deleteclient/{idClient}")
	public String deleteClient(@PathVariable(value = "idClient") Long idClient) {
			clientService.deleteClient(idClient);
		return "redirect:/client";
	}
}
	

