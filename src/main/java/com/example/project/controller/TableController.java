package com.example.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.project.entity.Tablee;
import com.example.project.service.TableeService;

@Controller
@RequestMapping()
public class TableController {

	@Autowired
	private TableeService tableeService;
	
	@GetMapping("/table")	
	public String viewTablePage(Model model) {
		
		List<Tablee> tablees = tableeService.findAllTablee();
		model.addAttribute("tablees", tablees);
		return "table/gestiontablees";
	}
	
	
	
	@GetMapping("/addtable")
	public String showTableForm(Model model) {
		Tablee tablee = new Tablee();
		model.addAttribute("tablee", tablee);
		return "table/tableform";
	}
	
	
	@PostMapping("/savetable")
	public String saveTable(@ModelAttribute("tablee")Tablee tablee) {
		
		tableeService.saveTablee(tablee);
		return "redirect:/table";
	}
	
	@GetMapping("/updatetable/{numTable}")
	public String showTableFormForUpdate(@PathVariable(value = "numTable")Long numTable , Model model) {
		
		Tablee tablee = tableeService.findTableById(numTable);
		model.addAttribute("tablee", tablee);
		
		return "table/tableupdate";
	}
	
	@GetMapping("/deletetable/{numTable}")
	public String deleteTable(@PathVariable(value = "numTable")Long numTable) {
		
		tableeService.deleteTable(numTable);
		return"redirect:/table";
	}
	
}
