package com.example.project.service;

import com.example.project.entity.Admin;

public interface AdminService {

	
	Admin findClientById(Long id);
}
