package com.example.project.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.project.entity.Tablee;
import com.example.project.repository.TableeRepository;

@Service
public class TableeServiceImpl implements TableeService {
	
	@Autowired
	private TableeRepository tableeRepository;

	@Override
	public List<Tablee> findAllTablee() {
		
		return tableeRepository.findAll();
	}

	@Override
	public Tablee saveTablee(Tablee tablee) {
		
		return this.tableeRepository.save(tablee);
	}

	@Override
	public Tablee findTableById(Long id) {
		Optional<Tablee> optional = tableeRepository.findById(id);
		Tablee tablee ;
		if(optional.isPresent())
		{
		   tablee = optional.get();
		}
		else
		{
			throw new RuntimeException(" !!! table not found for id :: " + id);
		}
		return tablee;
	}

	@Override
	public void deleteTable(Long id) {
		tableeRepository.deleteById(id);
		
	}

}
