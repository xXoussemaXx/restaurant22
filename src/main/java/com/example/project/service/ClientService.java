package com.example.project.service;

import java.util.List;

import com.example.project.entity.Client;

public interface ClientService {

	 List<Client> findAllClient();
	 Client saveClient(Client client);
	 Client findClientById(Long id);
	 void deleteClient(Long id);
	 
	 
	 
}
