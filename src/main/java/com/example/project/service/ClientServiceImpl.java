package com.example.project.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.project.entity.Client;
import com.example.project.repository.ClientRepository;

@Service
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientRepository repoClient ;
	
	

	@Override
	public List<Client> findAllClient() {
		return repoClient.findAll();
	}



	@Override
	public Client saveClient(Client client) {
		return  this.repoClient.save(client);
	}



	@Override
	public Client findClientById(Long id) {
		
		Optional<Client> optional = repoClient.findById(id);
		Client client =null;
		if(optional.isPresent()) {
			client = optional.get();
		}else {
			throw new  RuntimeException(" !!! client not found for id :: " + id);
		}
		
		return client;
	}



	@Override
	public void deleteClient(Long id) {
		repoClient.deleteById(id);
		
	}







}
