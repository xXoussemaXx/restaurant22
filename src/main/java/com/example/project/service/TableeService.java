package com.example.project.service;

import java.util.List;

import com.example.project.entity.Tablee;

public interface TableeService {

	List<Tablee> findAllTablee();
	Tablee saveTablee(Tablee tablee);
	 Tablee findTableById(Long id);
	 void deleteTable(Long id);
}
